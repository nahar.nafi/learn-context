import { useContext, useState } from 'react'

import { RootContext } from 'Context/RootContext'

function App() {
	const context = useContext(RootContext)
	const [biodata, setBiodata] = useState({ name: '', age: '' })

	const handleSubmit = (event) => {
		event.preventDefault()
		context.setBiodata(biodata)
	}

	return (
		<div>
			<h1>Learn Context</h1>
			<hr />
			<h2>Data From Global Store</h2>
			<div>
				Name: <b>{context.biodata.name}</b>
			</div>
			<div>
				Age: <b>{context.biodata.age}</b>
			</div>
			<hr />
			<h2>Data From Local Store</h2>
			<form onSubmit={handleSubmit}>
				<input type='text' value={biodata.name} placeholder='input your name' onChange={({ target: { value } }) => setBiodata({ ...biodata, name: value })} />
				<br />
				<input type='number' value={biodata.age} placeholder='input your age' onChange={({ target: { value } }) => setBiodata({ ...biodata, age: value })} />
				<br />
				<button type='submit'>Apply</button>
			</form>
		</div>
	)
}

export default App
