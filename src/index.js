import React from 'react'
import ReactDOM from 'react-dom'

import './index.css'
import App from './App'
import RootContextProvider from 'Context/RootContext'

ReactDOM.render(
	<React.StrictMode>
		<RootContextProvider>
			<App />
		</RootContextProvider>
	</React.StrictMode>,
	document.getElementById('root')
)
